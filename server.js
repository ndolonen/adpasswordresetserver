const express = require('express')
const cors = require('cors')
const app = express()
const port = 3000
// const adConnector = require("./adConnector.js")
app.use(cors())

//AD login Credentials
//Needs to be an admin
const adBruker = "ADMIN@snowcode.local"
const adPassord = "PASSORD"

//TODO: Change to ldaps!
let ldap = require('ldapjs')
var Client = ldap.createClient({
  url: 'ldap://127.0.0.1:389'
//   url: 'ldaps://127.0.0.1:636'
});

//listens for post requests on port, ideally would be something like /api/users
app.post('/', function(req, res) {
    console.log('POST /')
    //takes in user id and password as JSON
    let input = JSON.parse(req.headers.data)
    let resetIdentity = input.id
    let resetPassword = input.password
    
    changePassword(resetIdentity, resetPassword)
    
    res.writeHead(200, {'Content-Type': 'text/html'})
    res.end('Password reset')
})
  
app.listen(port, () => console.log(`Example app listening on port ${port}!`))

async function changePassword(Username,Password) {

    //TODO: Figure out of this is the right filter to use.
    let filter = '(uid=' + Username+ ')'

    function encodePassword(password) {
    return new Buffer('"' + password + '"', 'utf16le').toString();
    }
    
    Client.bind(adBruker, adPassord, function (err, result) {

        if (err) {
            console.error('error: ' + err);
        } else {
            Client.search('DC=snowcode,DC=local', {
                filter: filter,
                attributes: 'dn',
                scope: 'sub'
            }, function(err, res) {
                res.on('searchEntry', function(entry) {
                    var userDN = entry.object.dn;
                    Client.modify(userDN, [
                        //TODO: Replace needs ldaps. is there other possibilities?
                        new ldap.Change({
                        operation: 'replace',
                        modification: {
                            unicodePwd: encodePassword(Password)
                        }
                    })
                    ], function(err) {
                        if (err) {
                            console.log(err.code);
                            console.log(err.name);
                            console.log(err.message);
                            client.unbind();
                        }
                        else {
                            console.log('Password changed!');
                        }
                    });
                });
                res.on('error', function(err) {
                    console.error('error: ' + err.message);
                });
                res.on('end', function(result) {
                    console.log('status: ' + result.status);
                });
            });
        }
    });
}